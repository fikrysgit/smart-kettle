Projekt inteligentnej podstawki pod czajnik pozwalającej na ograniczenie zużycia prądu i wody. Głównym założeniem urządzenia jest możliwość działania z większością dostępnych czajników oraz bez fizycznej ingerencji w nie.

Niskim kosztem pozwala dodać pożądane funkcje, a także ograniczyć rachunki, dzięki gotowaniu odmierzonej ilości wody oraz funkcjom autonomicznym.